/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 11:35:36 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/11 12:03:48 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft.h"
# include "mlx.h"

# define WIN_WIDTH	2500
# define WIN_HEIGHT	1200
# define LINTER		WIN_HEIGHT - 70
# define MINTER		WIN_HEIGHT - 60
# define HINTER		WIN_HEIGHT - 50
# define DARK_BKG	0x002b36
# define LIGHT_BKG	0xF5DEB3
# define RED		0xB22222
# define SDATA		0x00b58b
# define IDATA		0x005f5e
# define GREEN		0x8dbe49
# define BKG_AOH	0x000000
# define SDATA_AOH	0x50B4D8
# define IDATA_AOH	0xEA3E54
# define GREEN_AOH	0x78BC65
# define ORANGE_AOH	0xEF7C2B
# define YELLOW_AOH	0xE7CE57

typedef struct		s_swap
{
	int				index;
	int				data;
	struct s_swap	*next;
}					t_swap;

typedef struct		s_inst
{
	char			*data;
	struct s_inst	*next;
}					t_inst;

typedef struct		s_mediane
{
	int					mediane;
	int					min;
	struct s_mediane	*next;
}					t_mediane;

typedef struct		s_colors
{
	int				bkg;
	int				i_data;
	int				s_data;
	int				green_txt;
	int				red_txt;
	int				gradient;
}					t_colors;

typedef struct		s_visu
{
	int				list_length;
	int				tmpa_length;
	int				max;
	int				min;
	int				moves;
	int				exit;
	int				max_moves;
	int				bpp;
	int				s_l;
	int				pause;
	int				next;
	int				prev;
	int				endian;
	long long		total;
	t_colors		colors;
	void			*good_job;
	void			*bad_job;
	void			*init;
	void			*window;
	void			*image;
	char			*img_addr;
	void			*send_hook;
}					t_visu;

/*
**	OPERATIONS FUNCTIONS
**	push.c:
**		- push
**		- distance
**	swap.c:
**		- swap_firsts
**		- swap_both
**	rotate.c
**		- rotate
**		- rotate_both
**		- reverse_rotate
**		- reverse_rotate_both
**	inst.c:
**		- push_inst
**		- rotate_inst
**		- swap_inst
**		- swap_both_inst
*/
int					push(t_swap **src, t_swap **dest);
int					distance(t_swap *list, int chr);
int					swap_firsts(t_swap **begin_list);
int					swap_both(t_swap **list1, t_swap **list2);
int					rotate(t_swap **begin_list);
int					rotate_both(t_swap **list1, t_swap **list2);
int					reverse_rotate(t_swap **begin_list);
int					reverse_rotate_both(t_swap **list1, t_swap **list2);
void				push_inst(t_swap **src, t_swap **dest,
								t_inst **inst, char *op);
void				rotate_inst(t_swap **list, t_swap **b,
								t_inst **inst, char *op);
void				swap_inst(t_swap **list, t_inst **inst, char *op);
void				swap_both_inst(t_swap **a, t_swap **b,
								t_inst **inst, char *op);

/*
**	VISUALIZER FUNCTIONS
**	visualizer.c:
**		- show_magic
**	print_data.c:
**		- print_cell_a
**		- print_cell_b
**	verbose.c:
**		- verbose_loop
**		- clean_params
**		- verbose_checker
*/
int					show_magic(t_swap **a, t_swap **b,
						char *str, t_visu *params);
void				if_verbose(t_swap **a, t_swap **b,
						t_inst **inst, t_visu *params);
void				print_data_a(t_swap *tmp, int pos, t_visu params);
void				print_data_b(t_swap *tmp, int pos, t_visu params);
int					verbose_loop(t_swap **a, t_swap **b,
								t_inst **inst, t_visu *params);
void				clean_params(t_visu *params, int emmergency);
int					verbose_checker(t_swap **a, t_swap **b,
								t_visu *params, t_inst **inst);

/*
**	MLX_TOOLS
**	mkx_tools.c:
**		- deal_key
**		- clear_pixels
**		- print_color
**		- interface
*/
int					deal_key(int key, t_visu *params);
void				clear_pixels(char *img);
void				print_color(char *img, int x, int y, int color);
void				interface(t_visu *params, char *str, t_swap *a, t_swap *b);

/*
**	SORT TOOLS
**	sort_tools.c
**		- decile_value
**		- next_at_start
**		- still_under_decile
**		- still_over_decile
*/
int					decile_value(int *tmp, int nb, int length);
int					next_at_start(t_swap **a, int chr);
int					still_under_decile(t_swap *list, int decile);
int					still_over_decile(t_swap *list, int decile);

/*
**	TAB FUNCTIONS
**	list_to_tab.c
**		- fill_tmp_tab
**		- there_is_inferior
*/
int					*fill_tmp_tab(t_swap *list, int sorted);
int					there_is_inferior(t_swap *list, int mediane);

/*
**	CLEAN FUNCTIONS
**	clean_inst.c:
**		- clean_pushes
**		- delete_pushes
**		- delete_inst
**		- clean_inst
*/
void				clean_pushes(t_inst **tmp, char *cmp1, char *cmp2);
void				delete_pushes(t_inst **tmp, int i, int j);
void				delete_inst(t_inst **tmp, int i);
void				clean_inst(t_inst **inst);
void				clean_rotation(t_inst **inst);

/*
**	CHECKS FUNCTIONS
**	list_checks.c:
**		- is_ascendant
**		- is_descendant
**		- final_check
**	checks.c:
**		- check_input
**		- check_doublons
**	gnl_chariot_return.c:
**		- gnl_chariot_return
*/
int					is_ascendant(t_swap *swap);
int					is_descendant(t_swap *swap);
int					final_check(t_swap *a, t_swap *b);
void				clean_tab(char **tab);
int					check_input(int ac, char **av);
int					check_doublons(t_swap *a);
int					gnl_chariot_return(const int fd, char **line);

/*
**	SORTING FUNCTIONS
**	basic_functions.c:
**		- basic_sort
**		- minmax_sort
**		- small_sort
*/
void				basic_sort(t_swap **a, t_swap **b, t_inst **inst);
void				merge_sort(t_swap **a, t_swap **b, t_inst **inst);
void				minmax_sort(t_swap **a, t_swap **b, t_inst **inst);
void				small_sort(t_swap **a, t_inst **inst);
void				bogosort(t_swap **a, t_swap **b, t_inst **inst);

/*
**	COPY FUNCTIONS
**	copy_tools.c
**		- copy_swap
**		- checker_tmp
**		- rewrite_inst
**		- check_instruction
*/
void				copy_swap(t_swap **src, t_swap *dest);
int					checker_tmp(t_swap *a, t_swap *b, t_inst *inst);
void				rewrite_inst(t_inst **dest, t_inst *src);
int					check_instruction(char *data, t_swap **a, t_swap **b);

/*
**	CHAINED LIST FUNCTIONS
**	swap_tools.c:
**		- ft_create_swap
**		- ft_push_swap
**		- ft_push_back_swap
**		- dellists
**		- ft_del_first_swap
**	instructions_tools.c:
**		- create_inst
**		- push_back_inst
**		- del_first_inst
**		- inst_length
**	list_creation.c:
**		- create_swap_list
**		- init_params
**	list_tools.c:
**		- list_length
**		- smallest_elem
**		- greatest_elem
**		- total_list
**		- at_start
*/
t_swap				*ft_create_swap(int data, int index);
void				ft_push_swap(t_swap **begin_list, int data, int index);
void				ft_push_back_swap(t_swap **begin_list, int data, int index);
void				dellists(t_swap **swap_a, t_swap **swap_b, t_inst **inst);
void				ft_del_first_swap(t_swap **begin_list);
t_inst				*create_inst(char *data);
void				push_back_inst(t_inst **begin_list, char *data);
void				del_first_inst(t_inst **begin_list);
int					inst_length(t_inst *inst);
void				create_swap_list(t_swap **a, char *str);
void				init_params(t_visu *params, t_swap *a);
int					list_length(t_swap *list);
int					smallest_elem(t_swap *list);
int					greatest_elem(t_swap *list);
long long			total_list(t_swap *list);
int					at_start(t_swap *list, int to_shr);

/*
**	COLORS FUNCTIONS
**	colors.c:
**		- change_color_mode
**		- init_xpm
**		- create_color_preset
*/
void				change_color_mode(t_visu *params, int mode);
int					init_xpm(t_visu *params);
t_colors			create_color_preset(void);

/*
**	PRINT FUNCTIONS
**	print_tools.c:
**		- ft_print_error
**		- print_instructions
*/
int					ft_print_error(void);
void				print_instructions(t_inst *inst);

#endif
