# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/04 18:02:25 by edjubert          #+#    #+#              #
#    Updated: 2019/04/10 14:55:44 by edjubert         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CHECKER			:=		checker
SWAP			:=		push_swap

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  DIRECTORIES                                 #

SRC_DIR			:=		./srcs
INC_DIR			:=		./includes
INC_LIB			:=		./libft/includes
INC_LIBX		:=		./minilibx_macos
OBJ_DIR			:=		./obj
LIBFT_FOLDER	:=		./libft
LIBFT			:=		$(LIBFT_FOLDER)/libft.a
LIBX_FOLDER		:=		./minilibx_macos
LIBX			:=		$(LIBX_FOLDER)/libmlx.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     FILES                                    #

SRC				:=		basic_sort.c			\
						checks.c				\
						clean_inst.c			\
						colors.c				\
						copy_tools.c			\
						gnl_chariot_return.c	\
						inst.c					\
						instructions_tools.c	\
						list_checks.c			\
						list_creation.c			\
						list_to_tab.c			\
						list_tools.c			\
						merge_sort.c			\
						minmax_sort.c			\
						mlx_tools.c				\
						print_data.c			\
						print_tools.c			\
						push.c					\
						rotate.c				\
						small_sort.c			\
						sort_tools.c			\
						swap.c					\
						swap_tools.c			\
						verbose.c				\
						visualizer.c

CHECKER_SRC		:=		checker.c

SWAP_SRC		:=		push_swap.c


OBJ				:=		$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
CHECKER_OBJ		:=		$(addprefix $(OBJ_DIR)/,$(CHECKER_SRC:.c=.o))
SWAP_OBJ		:=		$(addprefix $(OBJ_DIR)/,$(SWAP_SRC:.c=.o))
NB				:=		$(words $(SRC) $(CHECKER_SRC) $(SWAP_SRC))
INDEX			:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                               COMPILER & FLAGS                               #

GCC				:=		gcc
FLAGS			:=		-Wall					\
						-Wextra					\
						-g						\
						-Werror
LIBX_FLAGS		:=		-lmlx					\
						-framework OpenGL		\
						-framework AppKit

#==============================================================================#
#------------------------------------------------------------------------------#
#                                     RULES                                    #

all:				$(CHECKER) $(SWAP)

$(CHECKER):			$(OBJ) $(CHECKER_OBJ)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) --no-print-directory; fi
	@ if [ ! -f $(LIBX) ]; then make -C $(LIBX_FOLDER) --no-print-directory; fi
	@ $(GCC) $(FLAGS) $(LIBX_FLAGS) $(OBJ) $(CHECKER_OBJ) $(LIBFT) $(LIBX) -o $(CHECKER)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "Compilation of " $(CHECKER) " is done ---"

$(SWAP):			$(OBJ) $(SWAP_OBJ)
	@ if [ ! -f $(LIBFT) ]; then make -C $(LIBFT_FOLDER) --no-print-directory; fi
	@ if [ ! -f $(LIBX) ]; then make -C $(LIBX_FOLDER) --no-print-directory; fi
	@ $(GCC) $(FLAGS) $(LIBX_FLAGS) $(OBJ) $(SWAP_OBJ) $(LIBFT) $(LIBX) -o $(SWAP)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "Compilation of " $(SWAP) " is done ---"

$(OBJ_DIR)/%.o:		$(SRC_DIR)/%.c
	@ mkdir -p $(OBJ_DIR)
	@ $(eval DONE = $(shell echo $$(($(INDEX) * 20 / $(NB)))))
	@ $(eval PERCENT = $(shell echo $$(($(INDEX) * 100 / $(NB)))))
	@ $(eval TO_DO = $(shell echo "$@"))
	@ $(GCC) $(FLAGS) -I$(INC_LIB) -I$(INC_DIR) -I$(INC_LIBX) -c $< -o $@
	@ printf "\033[33m[ %3d%% ] %s\t%s\r\033[0m" $(PERCENT) $(CHECKER) $@
	@ $(eval INDEX = $(shell echo $$(($(INDEX) + 1))))

clean:
	@ /bin/rm -rf $(OBJ_DIR)
	@ make -C $(LIBFT_FOLDER) clean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "CLEAN  of " $(CHECKER) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "CLEAN  of " $(SWAP) " is done ---"

fclean:				clean
	@ /bin/rm -rf $(CHECKER) $(SWAP)
	@ make -C $(LIBFT_FOLDER) fclean --no-print-directory
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "FCLEAN of " $(CHECKER) " is done ---"
	@ printf '\033[91m[ KILL ] %-15s\033[31m%-30s\033[91m%s\n\033[0m' "FCLEAN of " $(SWAP) " is done ---"

re:				fclean all

rebuild:		$(OBJ) $(CHECKER_OBJ) $(SWAP_OBJ)
	@ rm -rf $(CHECKER) $(SWAP)
	@ $(GCC) $(FLAGS) $(LIBX_FLAGS) $(OBJ) $(CHECKER_OBJ) $(LIBFT) $(LIBX) -o $(CHECKER)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "REBUILD of " $(CHECKER) " is done ---"
	@ $(GCC) $(FLAGS) $(LIBX_FLAGS) $(OBJ) $(SWAP_OBJ) $(LIBFT) $(LIBX) -o $(SWAP)
	@ printf '\033[32m[ 100%% ] %-15s\033[92m%-30s\033[32m%s\n\033[0m' "REBUILD of " $(SWAP) " is done ---"

.PHONY: all clean fclean re
