/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_data.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 12:23:12 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/28 14:31:32 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		print_cell_b(int x[2], double size[3], int pos, t_visu params)
{
	if ((x[0] == (((WIN_WIDTH / 2) + ((WIN_WIDTH) - size[0])) / 2)
				+ size[0] - 1 || x[1] == size[1]
				+ (size[1] * pos) - 1) && size[1] > 1)
		print_color(params.img_addr, x[0], x[1], params.colors.bkg);
	else if (size[2] < 0)
		print_color(params.img_addr, x[0], x[1], params.colors.i_data
				+ (params.colors.gradient ? ((int)size[0] / 5) + 2 : 0));
	else
		print_color(params.img_addr, x[0], x[1], params.colors.s_data
				+ (params.colors.gradient ? ((int)size[0] / 5) + 2 : 0));
}

static void		print_cell_a(int x[2], double size[3], int start, t_visu params)
{
	if (x[1] == start + size[1] && size[1] > 1)
		print_color(params.img_addr, x[0], x[1], params.colors.bkg);
	else if (size[2] < 0)
		print_color(params.img_addr, x[0], x[1], params.colors.i_data
				+ (params.colors.gradient ? ((int)size[0] / 5) + 2 : 0));
	else
		print_color(params.img_addr, x[0], x[1], params.colors.s_data
				+ (params.colors.gradient ? ((int)size[0] / 5) + 2 : 0));
}

void			print_data_b(t_swap *tmp, int pos, t_visu params)
{
	double	size[3];
	int		x[2];

	size[0] = ((double)tmp->data / (double)params.max) * WIN_WIDTH / 2;
	size[0] = size[0] < 0 ? size[0] * -1 : size[0];
	size[0] = size[0] < 2 ? 2 : size[0];
	size[1] = (WIN_HEIGHT - 100) / (params.list_length < 1
			? 1 : params.list_length);
	size[1] = size[1] < 1 ? 1 : size[1];
	x[1] = size[1] * pos;
	while (x[1] < WIN_HEIGHT && x[1] < size[1] + (size[1] * pos))
	{
		x[0] = ((WIN_WIDTH / 2) + ((WIN_WIDTH) - size[0])) / 2;
		while (x[0] < (((WIN_WIDTH / 2)
						+ ((WIN_WIDTH) - size[0])) / 2) + size[0])
		{
			size[2] = tmp->data;
			print_cell_b(x, size, pos, params);
			x[0]++;
		}
		x[1]++;
	}
}

void			print_data_a(t_swap *tmp, int pos, t_visu params)
{
	double	size[3];
	int		start;
	int		x[2];

	size[0] = ft_abs(((double)tmp->data / (double)params.max) * WIN_WIDTH / 2);
	size[0] = size[0] < 0 ? size[0] * -1 : size[0];
	size[0] = size[0] < 2 ? 2 : size[0];
	size[1] = (WIN_HEIGHT - 100) / (params.list_length < 1
			? 1 : params.list_length);
	size[1] = size[1] < 1 ? 1 : size[1];
	start = size[1] * (params.list_length - (params.tmpa_length - pos));
	x[1] = start + size[1];
	size[2] = tmp->data;
	while (x[1] < WIN_HEIGHT && x[1] > start)
	{
		x[0] = ((WIN_WIDTH / 2) - size[0]) / 2;
		while (x[0] < (((WIN_WIDTH / 2) - size[0]) / 2) + size[0])
		{
			print_cell_a(x, size, start, params);
			x[0]++;
		}
		x[1]--;
	}
}
