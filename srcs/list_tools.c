/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 13:10:46 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/14 19:17:56 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			list_length(t_swap *list)
{
	int		i;
	t_swap	*tmp;

	i = 1;
	tmp = list;
	while (tmp && tmp->next)
	{
		tmp = tmp->next;
		i++;
	}
	return (i);
}

int			smallest_elem(t_swap *list)
{
	int	ret;

	ret = 2147483647;
	while (list)
	{
		if (ret > list->data)
			ret = list->data;
		list = list->next;
	}
	return (ret);
}

int			greatest_elem(t_swap *list)
{
	int	ret;

	ret = -2147483648;
	while (list)
	{
		if (ret < list->data)
			ret = list->data;
		list = list->next;
	}
	return (ret);
}

long long	total_list(t_swap *list)
{
	long long	ret;

	ret = 0;
	while (list)
	{
		ret += list->data < 0 ? list->data * -1 : list->data;
		list = list->next;
	}
	return (ret);
}

int			at_start(t_swap *list, int to_shr)
{
	int	length;

	length = 0;
	while (list && list->data != to_shr)
	{
		list = list->next;
		length++;
	}
	return (length > list_length(list) / 2 ? 0 : 1);
}
