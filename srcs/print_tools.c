/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 11:58:43 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 16:23:01 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_print_error(void)
{
	ft_putendl("Error");
	return (-1);
}

void	print_instructions(t_inst *inst)
{
	while (inst)
	{
		if (inst->data[0] != '\0')
			ft_printf("%s\n", inst->data);
		if (inst)
			inst = inst->next;
	}
}
