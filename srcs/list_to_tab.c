/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_to_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 12:07:47 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/29 13:27:55 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sort_tmp_tab(int *tmp, int length, int ascendant)
{
	int	tmp_val;
	int	i;

	i = 0;
	while (i < length - 1)
	{
		if (ascendant && tmp[i] > tmp[i + 1])
		{
			tmp_val = tmp[i];
			tmp[i] = tmp[i + 1];
			tmp[i + 1] = tmp_val;
			i = 0;
		}
		else if (!ascendant && tmp[i] < tmp[i + 1])
		{
			tmp_val = tmp[i];
			tmp[i] = tmp[i + 1];
			tmp[i + 1] = tmp_val;
			i = 0;
		}
		else
			i++;
	}
}

int			*fill_tmp_tab(t_swap *list, int sorted)
{
	int	i;
	int	*tmp;
	int	length;

	length = list_length(list);
	if (!(tmp = (int*)malloc(sizeof(int) * length)))
		return (NULL);
	i = 0;
	while (list && i < length)
	{
		tmp[i] = list->data;
		list = list->next;
		i++;
	}
	if (sorted)
		sort_tmp_tab(tmp, length, 1);
	return (tmp);
}

int			there_is_inferior(t_swap *list, int mediane)
{
	t_swap	*tmp;

	tmp = list;
	while (tmp)
	{
		if (tmp->data < mediane)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
