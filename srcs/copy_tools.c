/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/01 10:31:20 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/01 17:28:48 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	clean_swap(t_swap **list)
{
	while (*list)
	{
		free(*list);
		*list = (*list)->next;
	}
}

void		copy_swap(t_swap **src, t_swap *dest)
{
	t_swap	*tmp;
	int		i;

	while (*src)
	{
		(*src)->data = 0;
		(*src)->index = 0;
		free(*src);
		*src = (*src)->next;
	}
	i = 0;
	tmp = dest;
	while (tmp)
	{
		ft_push_back_swap(src, tmp->data, i);
		tmp = tmp->next;
		i++;
	}
}

int			checker_tmp(t_swap *a, t_swap *b,
		t_inst *inst)
{
	int		ret;
	t_inst	*tmp;
	t_swap	*tmpa;
	t_swap	*tmpb;

	tmp = inst;
	tmpa = NULL;
	tmpb = NULL;
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	while (tmp)
	{
		if (check_instruction(tmp->data, &tmpa, &tmpb) == -1)
			return (-1);
		tmp = tmp->next;
	}
	ret = final_check(tmpa, tmpb);
	clean_swap(&tmpa);
	clean_swap(&tmpb);
	return (ret);
}

void		rewrite_inst(t_inst **dest, t_inst *src)
{
	while (*dest)
	{
		free(*dest);
		*dest = (*dest)->next;
	}
	while (src)
	{
		push_back_inst(dest, src->data);
		src = src->next;
	}
}

int			check_instruction(char *data, t_swap **a, t_swap **b)
{
	if (ft_strcmp(data, "sa") == 0)
		return (swap_firsts(a));
	else if (ft_strcmp(data, "sb") == 0)
		return (swap_firsts(b));
	else if (ft_strcmp(data, "ss") == 0)
		return (swap_both(a, b));
	else if (ft_strcmp(data, "pa") == 0)
		return (push(b, a));
	else if (ft_strcmp(data, "pb") == 0)
		return (push(a, b));
	else if (ft_strcmp(data, "ra") == 0)
		return (rotate(a));
	else if (ft_strcmp(data, "rb") == 0)
		return (rotate(b));
	else if (ft_strcmp(data, "rr") == 0)
		return (rotate_both(a, b));
	else if (ft_strcmp(data, "rra") == 0)
		return (reverse_rotate(a));
	else if (ft_strcmp(data, "rrb") == 0)
		return (reverse_rotate(b));
	else if (ft_strcmp(data, "rrr") == 0)
		return (reverse_rotate_both(a, b));
	else
		return (data[0] > 0 ? -1 : 0);
}
