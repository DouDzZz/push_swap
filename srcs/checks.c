/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 13:44:08 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/04 18:07:15 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	check_int(char *str)
{
	if (ft_strlen(str) > 3 && (ft_atol(str) == 0 || ft_atol(str) == -1
			|| ft_atol(str) > 2147483647 || ft_atol(str) < -2147483648))
		return (0);
	return (1);
}

static int	check_param(char *str)
{
	int	i;

	i = 0;
	if (str[0] == '-' || str[0] == '+')
		i = 1;
	while (str[i])
		if (!ft_isdigit(str[i++]))
			return (0);
	if (!check_int(str))
		return (0);
	return (1);
}

int			check_doublons(t_swap *a)
{
	int		i;
	t_swap	*tmp;
	t_swap	*checker;

	tmp = a;
	while (tmp)
	{
		checker = a;
		i = 0;
		while (checker)
		{
			if (tmp->data == checker->data)
				i++;
			if (i > 1)
				return (0);
			checker = checker->next;
		}
		tmp = tmp->next;
	}
	return (1);
}

static int	check_arguments(int ac, char **av)
{
	if (ac < 2)
		return (0);
	else if (ac == 2 && ft_strcmp(av[1], "-v") == 0)
		return (0);
	else if (ft_strcmp(av[1], "-v") == 0)
		return (1);
	else
		return (2);
}

int			check_input(int ac, char **av)
{
	int		i;
	int		j;
	int		should_free;
	char	**params;

	i = 0;
	if ((i = check_arguments(ac, av)) < 1)
		return (i);
	i = i == 2 ? 0 : i;
	while (++i < ac)
	{
		j = -1;
		should_free = ft_strlen(av[i]) == 0 ? 0 : 1;
		params = ft_strsplit(av[i], ' ');
		while (should_free && params[++j] != 0)
		{
			if (!check_param(params[j]))
			{
				clean_tab(params);
				return (-1);
			}
		}
		should_free ? clean_tab(params) : NULL;
	}
	return (1);
}
