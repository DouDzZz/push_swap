/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 11:06:05 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 14:14:20 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	no_gradient_modes(t_visu *params, int mode)
{
	params->colors.green_txt = GREEN;
	params->colors.red_txt = RED;
	params->colors.gradient = 0;
	if (mode == 1)
	{
		params->colors.i_data = IDATA;
		params->colors.s_data = SDATA;
	}
	else if (mode == 2 || mode == 3)
	{
		params->colors.i_data = mode == 2 ? IDATA + 45 : IDATA - 45;
		params->colors.s_data = mode == 2 ? SDATA + 45 : SDATA - 45;
	}
	else if (mode == 4)
	{
		params->colors.i_data = IDATA_AOH;
		params->colors.s_data = SDATA_AOH;
		params->colors.green_txt = YELLOW_AOH;
		params->colors.red_txt = ORANGE_AOH;
	}
}

void		change_color_mode(t_visu *params, int mode)
{
	int	light;

	light = LIGHT_BKG;
	if (mode == 6 || mode == 10)
	{
		params->colors.i_data = ORANGE_AOH - 45;
		params->colors.s_data = ORANGE_AOH - 45;
		params->colors.green_txt = YELLOW_AOH;
		params->colors.red_txt = ORANGE_AOH;
		params->colors.gradient = 1;
	}
	else if (mode >= 1 && mode <= 4)
		no_gradient_modes(params, mode);
	else if (mode == 11)
		params->colors.bkg = params->colors.bkg == light ? DARK_BKG : LIGHT_BKG;
}

int			init_xpm(t_visu *params)
{
	int		x[2];
	int		y[2];
	void	*good_addr;

	x[0] = 0;
	x[1] = 0;
	y[0] = 0;
	y[1] = 0;
	good_addr = mlx_xpm_file_to_image(params->init, "good.xpm", x, y);
	params->good_job = good_addr;
	params->bad_job = mlx_xpm_file_to_image(params->init, "bad.xpm", x, y);
	if (params->good_job == NULL)
		return (-1);
	else
		return (1);
}

t_colors	create_color_preset(void)
{
	t_colors	new;

	new.bkg = DARK_BKG;
	new.i_data = IDATA;
	new.s_data = SDATA;
	new.green_txt = GREEN;
	new.red_txt = RED;
	new.gradient = 0;
	return (new);
}
