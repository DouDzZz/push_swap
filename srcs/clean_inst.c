/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_inst.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 16:39:28 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/09 19:55:08 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	delete_inst(t_inst **tmp, int i)
{
	while (i)
	{
		(*tmp)->data = "";
		*tmp = (*tmp)->next;
		i--;
	}
}

void	delete_pushes(t_inst **tmp, int i, int j)
{
	if (i && j)
	{
		if (i > j)
		{
			i = i - j;
			while (i)
			{
				*tmp = (*tmp)->next;
				i--;
			}
			delete_inst(tmp, j);
		}
		else
		{
			j = i;
			delete_inst(tmp, i);
			delete_inst(tmp, j);
		}
	}
}

void	clean_pushes(t_inst **tmp, char *cmp1, char *cmp2)
{
	int		i;
	int		j;
	t_inst	*runner;

	i = 0;
	runner = *tmp;
	while (runner && ft_strcmp(runner->data, cmp1) == 0)
	{
		runner = runner->next;
		i++;
	}
	j = 0;
	while (runner && ft_strcmp(runner->data, cmp2) == 0)
	{
		runner = runner->next;
		j++;
	}
	delete_pushes(tmp, i, j);
}

void	clean_inst(t_inst **inst)
{
	t_inst	*tmp;

	tmp = *inst;
	while (tmp)
	{
		if (ft_strcmp(tmp->data, "sb") == 0
				&& ft_strcmp(tmp->next->data, "sa") == 0)
		{
			tmp->data = "ss";
			tmp->next = tmp->next->next;
		}
		else if (ft_strcmp(tmp->data, "sa") == 0
				&& ft_strcmp(tmp->next->data, "sb") == 0)
		{
			tmp->data = "ss";
			tmp->next = tmp->next->next;
		}
		if (tmp->next && ft_strcmp(tmp->data, "pb") == 0)
			clean_pushes(&tmp, "pb", "pa");
		if (tmp && tmp->next && ft_strcmp(tmp->data, "pa") == 0)
			clean_pushes(&tmp, "pa", "pb");
		if (tmp)
			tmp = tmp->next;
	}
}

void	clean_rotation(t_inst **inst)
{
	t_inst	*tmp;

	tmp = *inst;
	while (tmp)
	{
		if (tmp && tmp->next && ft_strcmp(tmp->data, "rb") == 0
				&& ft_strcmp(tmp->next->data, "ra") == 0)
		{
			tmp->data = "rr";
			free(tmp->next);
			tmp->next = tmp->next->next;
		}
		else if (ft_strcmp(tmp->data, "ra") == 0
				&& ft_strcmp(tmp->next->data, "rb") == 0)
		{
			tmp->data = "rr";
			free(tmp->next);
			tmp->next = tmp->next->next;
		}
		if (tmp)
			tmp = tmp->next;
	}
}
