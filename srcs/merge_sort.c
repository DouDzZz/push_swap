/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 14:37:30 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/01 18:18:10 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		until_ascension(t_swap **a, t_swap **b, t_inst **inst)
{
	while (!is_ascendant(*a))
	{
		while ((*a)->data != smallest_elem(*a))
			rotate_inst(a, NULL, inst,
					at_start(*a, smallest_elem(*b)) ? "ra" : "rra");
		if ((*a)->data == smallest_elem(*a))
			push_inst(a, b, inst, "pb");
		if (*b && (*b)->next && (*b)->data < (*b)->next->data)
			swap_inst(b, inst, "sb");
	}
}

static void		fill_to_b(t_swap **a, t_swap **b, t_inst **inst, int *tmp)
{
	int		i;
	int		length;

	i = 1;
	length = list_length(*a);
	while (*a && i < length)
	{
		while (still_under_decile(*a, decile_value(tmp, i, length)))
		{
			if (*a && (*a)->data <= decile_value(tmp, i, length))
				push_inst(a, b, inst, "pb");
			else if (next_at_start(a, decile_value(tmp, i, length)))
				rotate_inst(a, NULL, inst, "ra");
			else if (!next_at_start(a, decile_value(tmp, i, length)))
				rotate_inst(a, NULL, inst, "rra");
		}
		if (list_length(*a) < 2)
			return ;
		i++;
	}
	until_ascension(a, b, inst);
}

static void		fill_to_a(t_swap **a, t_swap **b, t_inst **inst)
{
	int	*tmp;
	int	length;

	tmp = fill_tmp_tab(*b, 1);
	length = list_length(*b);
	while (*b)
	{
		while ((*b)->data != greatest_elem(*b))
			rotate_inst(b, NULL, inst, at_start(*b, greatest_elem(*b))
					? "rb" : "rrb");
		if ((*b)->data == greatest_elem(*b))
			push_inst(b, a, inst, "pa");
		if (*a && (*a)->next && (*a)->data > (*a)->next->data)
			swap_inst(a, inst, "sa");
	}
	free(tmp);
}

void			merge_sort(t_swap **a, t_swap **b, t_inst **inst)
{
	int	*tmp;

	tmp = fill_tmp_tab(*a, 1);
	fill_to_b(a, b, inst, tmp);
	fill_to_a(a, b, inst);
	if (!final_check(*a, *b))
		merge_sort(a, b, inst);
	free(tmp);
}
