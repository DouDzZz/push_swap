/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 17:16:51 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/18 18:27:51 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	rotate(t_swap **begin_list)
{
	int		first;
	t_swap	*tmp;

	if (*begin_list == NULL || (*begin_list)->next == NULL)
		return (0);
	first = (*begin_list)->data;
	tmp = *begin_list;
	while (tmp && tmp->next)
	{
		tmp->data = tmp->next->data;
		tmp = tmp->next;
	}
	tmp->data = first;
	return (1);
}

int	rotate_both(t_swap **list1, t_swap **list2)
{
	rotate(list1);
	rotate(list2);
	return (1);
}

int	reverse_rotate(t_swap **begin_list)
{
	int		tmp1;
	int		tmp2;
	t_swap	*tmp;

	if (*begin_list == NULL || (*begin_list)->next == NULL)
		return (0);
	tmp = *begin_list;
	while (tmp && tmp->next)
		tmp = tmp->next;
	tmp1 = tmp->data;
	tmp = *begin_list;
	while (tmp && tmp->next)
	{
		tmp2 = tmp->data;
		tmp->data = tmp1;
		tmp1 = tmp2;
		tmp = tmp->next;
	}
	tmp->data = tmp1;
	return (1);
}

int	reverse_rotate_both(t_swap **list1, t_swap **list2)
{
	reverse_rotate(list1);
	reverse_rotate(list2);
	return (1);
}
