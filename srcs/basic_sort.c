/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basic_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 17:38:43 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/25 10:31:04 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		push_if_found(t_swap **a, t_swap **b,
							t_inst **inst, int smallest)
{
	if ((*a)->data == smallest)
		push_inst(a, b, inst, "pb");
	else if (!at_start(*a, smallest))
		while ((*a)->data != smallest)
			rotate_inst(a, NULL, inst, "rra");
	else
	{
		while ((*a)->data != smallest)
			rotate_inst(a, NULL, inst, "ra");
	}
}

static void		hide_and_seek(t_swap **a, t_swap **b, t_inst **inst)
{
	int	smallest;

	smallest = smallest_elem(*a);
	push_if_found(a, b, inst, smallest);
	basic_sort(a, b, inst);
}

void			basic_sort(t_swap **a, t_swap **b, t_inst **inst)
{
	if ((*a)->data != smallest_elem(*a))
		hide_and_seek(a, b, inst);
	else if (is_ascendant(*a) || !*a)
		while (*b)
			push_inst(b, a, inst, "pa");
	else if ((*a)->data == smallest_elem(*a))
		push_inst(a, b, inst, "pb");
	if (!final_check(*a, *b))
		basic_sort(a, b, inst);
}
