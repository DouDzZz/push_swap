/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 11:29:33 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 16:25:56 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		no_verbose_loop(t_swap **a, t_swap **b, t_inst **inst)
{
	t_inst	*tmp;

	tmp = *inst;
	if (check_instruction(tmp->data, a, b) == -1)
		return (-1);
	*inst = (*inst)->next;
	free(tmp->data);
	free(tmp);
	return (1);
}

static int		write_instructions(t_inst **inst)
{
	char	*str;

	while (gnl_chariot_return(0, &str) > 0)
	{
		if (ft_strcmp(str, "sa\n") && ft_strcmp(str, "sb\n")
				&& ft_strcmp(str, "ss\n") && ft_strcmp(str, "pa\n")
				&& ft_strcmp(str, "sb\n") && ft_strcmp(str, "pb\n")
				&& ft_strcmp(str, "ra\n") && ft_strcmp(str, "rb\n")
				&& ft_strcmp(str, "rr\n") && ft_strcmp(str, "rra\n")
				&& ft_strcmp(str, "rrb\n") && ft_strcmp(str, "rrr\n"))
			return (-1);
		push_back_inst(inst, ft_strsub(str, 0, ft_strlen(str) - 1));
		ft_strdel(&str);
	}
	return (1);
}

static int		checker(t_swap **a, t_swap **b,
		t_inst **instructions, int verbose)
{
	int		ret;
	t_visu	params;

	if (verbose == 1)
		init_params(&params, *a);
	if ((params.init == NULL
				|| params.good_job == NULL
				|| params.bad_job == NULL) && verbose == 1)
		return (-1);
	if (write_instructions(instructions) == -1)
		return (-1);
	if (verbose)
	{
		if ((ret = verbose_checker(a, b, &params, instructions)) < 1)
			return (ret);
	}
	else
		while (*instructions)
			if ((ret = no_verbose_loop(a, b, instructions)) < 1)
				return (ret);
	if (verbose)
		mlx_hook(params.window, 2, 1L << 0, deal_key, &params);
	verbose ? clean_params(&params, 0) : sleep(0);
	return (0);
}

int				main(int ac, char **av)
{
	int		i;
	int		verbose;
	t_swap	*a;
	t_swap	*b;
	t_inst	*instructions;

	if (ac == 1 || (ac == 2 && ft_strcmp(av[1], "-v") == 0))
		return (0);
	if ((i = check_input(ac, av)) < 0)
		return (i < 0 ? ft_print_error() : 0);
	i = ft_strcmp(av[1], "-v") == 0 ? 1 : 0;
	a = NULL;
	b = NULL;
	instructions = NULL;
	verbose = i;
	while (++i < ac)
		create_swap_list(&a, av[i]);
	if (!check_doublons(a))
		return (ft_print_error());
	if (checker(&a, &b, &instructions, verbose) == -1)
		return (ft_print_error());
	final_check(a, b) ? ft_printf("OK\n") : ft_printf("KO\n");
	dellists(&a, &b, &instructions);
	return (0);
}
