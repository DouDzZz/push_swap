/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_checks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 13:31:00 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/03 16:26:55 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		is_ascendant(t_swap *swap)
{
	while (swap && swap->next)
	{
		if (swap->data > swap->next->data)
			return (0);
		swap = swap->next;
	}
	return (1);
}

int		is_descendant(t_swap *swap)
{
	while (swap && swap->next)
	{
		if (swap->data < swap->next->data)
			return (0);
		swap = swap->next;
	}
	return (1);
}

int		final_check(t_swap *a, t_swap *b)
{
	if (b != NULL)
		return (0);
	return (is_ascendant(a));
}

void	clean_tab(char **tab)
{
	int	i;

	i = 0;
	while (tab[i])
	{
		ft_strdel(&tab[i]);
		i++;
	}
	if (tab[i])
		ft_strdel(&tab[i]);
	if (tab)
		free(tab);
}
