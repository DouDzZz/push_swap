/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 16:10:29 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/08 16:42:01 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	swap_firsts(t_swap **begin_list)
{
	int	tmp;

	if (*begin_list == NULL || (*begin_list)->next == NULL)
		return (0);
	tmp = (*begin_list)->next->data;
	(*begin_list)->next->data = (*begin_list)->data;
	(*begin_list)->data = tmp;
	return (1);
}

int	swap_both(t_swap **list1, t_swap **list2)
{
	swap_firsts(list1);
	swap_firsts(list2);
	return (1);
}
