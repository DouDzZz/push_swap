/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verbose.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 17:41:42 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 14:53:46 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		verbose_loop(t_swap **a, t_swap **b,
		t_inst **inst, t_visu *params)
{
	if (!params->pause
			&& check_instruction((*inst)->data, a, b) == -1)
		return (-1);
	if (params->exit)
		return (0);
	if_verbose(a, b, inst, params);
	return (1);
}

void	clean_params(t_visu *params, int emmergency)
{
	if (!emmergency)
		sleep(10);
	mlx_destroy_image(params->init, params->image);
	mlx_destroy_image(params->init, params->good_job);
	mlx_destroy_image(params->init, params->bad_job);
	mlx_destroy_window(params->init, params->window);
	mlx_del(params->init);
}

int		verbose_checker(t_swap **a, t_swap **b,
						t_visu *params, t_inst **inst)
{
	int	ret;

	params->max_moves = inst_length(*inst);
	while (*inst)
	{
		if ((ret = verbose_loop(a, b, inst, params)) < 1)
		{
			while (*inst)
			{
				free((*inst)->data);
				free(*inst);
				*inst = (*inst)->next;
			}
			clean_params(params, 1);
			return (ret);
		}
	}
	return (1);
}
