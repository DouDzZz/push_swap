/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualizer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 10:32:46 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 15:21:02 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		fill_back(char *img, int color)
{
	int	x;
	int	y;
	int	r;
	int	g;
	int	b;

	y = 0;
	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	clear_pixels(img);
	while (y < WIN_HEIGHT)
	{
		x = 0;
		while (x < WIN_WIDTH)
		{
			img[((x * 4) + 4 * WIN_WIDTH * y) + 0] = b;
			img[((x * 4) + 4 * WIN_WIDTH * y) + 1] = g;
			img[((x * 4) + 4 * WIN_WIDTH * y) + 2] = r;
			x++;
		}
		y++;
	}
}

void			if_verbose(t_swap **a, t_swap **b,
					t_inst **inst, t_visu *params)
{
	t_inst	*tmp;

	show_magic(a, b, (*inst)->data, params);
	if (params->next && !params->pause)
	{
		params->pause = 1;
		params->next = 0;
	}
	if (!params->pause || params->next)
	{
		tmp = *inst;
		*inst = (*inst)->next;
		free(tmp->data);
		free(tmp);
		params->moves++;
	}
	if (params->next)
		params->pause = 0;
	if (!final_check(*a, *b) && !*inst)
		mlx_put_image_to_window(params->init,
		params->window, params->bad_job, WIN_WIDTH / 2 + WIN_WIDTH / 10, 50);
	mlx_do_sync(params->init);
}

int				deal_key(int key, t_visu *params)
{
	if (key == 53)
		params->exit = 1;
	if (key == 49)
		params->pause = params->pause ? 0 : 1;
	if (key == 124)
	{
		params->next = 1;
		params->pause = 1;
	}
	if (key >= 18 && key <= 27)
		change_color_mode(params, key - 17);
	if (key == 11)
		change_color_mode(params, 11);
	if (key == 5)
		change_color_mode(params, 10);
	return (0);
}

int				show_magic(t_swap **a, t_swap **b, char *str, t_visu *params)
{
	int		i;
	t_swap	*tmpa;
	t_swap	*tmpb;

	tmpa = *a;
	tmpb = *b;
	i = 0;
	fill_back(params->img_addr, params->colors.bkg);
	params->tmpa_length = list_length(tmpa);
	while (tmpa)
	{
		if (tmpa)
			print_data_a(tmpa, i, *params);
		i++;
		tmpa = tmpa->next;
	}
	while (tmpb)
	{
		if (tmpb)
			print_data_b(tmpb, i, *params);
		i++;
		tmpb = tmpb->next;
	}
	interface(params, str, *a, *b);
	return (1);
}
