/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 14:06:34 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/03 17:48:28 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_swap	*ft_create_swap(int data, int index)
{
	t_swap	*new;

	if (!(new = (t_swap*)malloc(sizeof(t_swap))))
		return (NULL);
	new->data = data;
	new->index = index;
	new->next = NULL;
	return (new);
}

void	ft_push_swap(t_swap **begin_list, int data, int index)
{
	t_swap	*new;

	if (*begin_list == NULL)
	{
		*begin_list = ft_create_swap(data, index);
		return ;
	}
	new = ft_create_swap(data, index);
	new->next = *begin_list;
	*begin_list = new;
}

void	ft_push_back_swap(t_swap **begin_list, int data, int index)
{
	t_swap	*new;
	t_swap	*tmp;

	tmp = *begin_list;
	if (tmp == NULL)
	{
		*begin_list = ft_create_swap(data, index);
		return ;
	}
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = ft_create_swap(data, index);
	tmp->next = new;
}

void	dellists(t_swap **swap_a, t_swap **swap_b, t_inst **inst)
{
	while ((*swap_a))
	{
		(*swap_a)->data = 0;
		(*swap_a)->index = 0;
		free(*swap_a);
		*swap_a = (*swap_a)->next;
	}
	while ((*swap_b))
	{
		(*swap_b)->data = 0;
		(*swap_b)->index = 0;
		free(*swap_b);
		*swap_b = (*swap_b)->next;
	}
	while ((*inst))
	{
		free(*inst);
		*inst = (*inst)->next;
	}
}

void	ft_del_first_swap(t_swap **begin_list)
{
	t_swap	*tmp;

	if (*begin_list == NULL)
		return ;
	tmp = (*begin_list);
	*begin_list = (*begin_list)->next;
	free(tmp);
}
