/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_creation.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 11:02:07 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/10 15:21:09 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		create_swap_list(t_swap **a, char *str)
{
	int		i;
	char	**params;

	i = 0;
	params = ft_strsplit(str, ' ');
	if (params == NULL)
		return ;
	while (params[i])
	{
		if (ft_strlen_pro(params[i]) > 0)
			ft_push_back_swap(a, ft_atoi(params[i]), i);
		ft_strdel(&params[i]);
		i++;
	}
	ft_strdel(&params[i]);
	free(params);
}

void		init_params(t_visu *params, t_swap *a)
{
	params->init = mlx_init();
	if (params->init == NULL)
		return ;
	params->window = mlx_new_window(params->init,
			WIN_WIDTH, WIN_HEIGHT, "Push Swap");
	params->image = mlx_new_image(params->init, WIN_WIDTH, WIN_HEIGHT);
	params->img_addr = mlx_get_data_addr(params->image,
		&params->bpp, &params->s_l, &params->endian);
	params->list_length = list_length(a);
	if (init_xpm(params) == -1)
		return ;
	params->colors = create_color_preset();
	params->pause = 0;
	params->max_moves = 0;
	params->exit = 0;
	params->next = 0;
	params->prev = 0;
	params->max = greatest_elem(a);
	params->min = smallest_elem(a);
	params->total = total_list(a);
	params->moves = 0;
}
