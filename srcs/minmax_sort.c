/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minmax_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 14:37:30 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/11 12:05:50 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		cal_mediane(int length)
{
	int	i;

	if (length < 50)
		i = 1 + length / 10;
	else if (length < 100)
		i = 2 + length / 10;
	else if (length < 150)
		i = 8 + length / 100;
	else if (length < 300)
		i = 12 + length / 100;
	else
		i = 13 + length / 100;
	i = (i == 0) ? 1 : i;
	return (i);
}

static void		fill_to_b(t_swap **a, t_swap **b, t_inst **inst, int *tmp)
{
	int		i;
	int		length;

	length = list_length(*a);
	while (*a)
	{
		tmp = fill_tmp_tab(*a, 1);
		i = cal_mediane(list_length(*a));
		if (*a && (*a)->data <= tmp[i])
			push_inst(a, b, inst, "pb");
		else if (*a && (*a)->data >= tmp[list_length(*a) - i - 1])
		{
			push_inst(a, b, inst, "pb");
			rotate_inst(b, NULL, inst, "rb");
		}
		else
			rotate_inst(a, NULL, inst, "ra");
		free(tmp);
	}
}

static void		check_swap(t_swap **a, t_swap **b, t_inst **inst)
{
	if (*a && (*a)->next && (*a)->data > (*a)->next->data)
	{
		if (*b && (*b)->next && (*b)->data < (*b)->next->data)
			swap_both_inst(a, b, inst, "ss");
		else
			swap_inst(a, inst, "sa");
	}
}

static void		fill_to_a(t_swap **a, t_swap **b, t_inst **inst)
{
	int	*tmp;
	int	sg;
	int	length;
	int	next;
	int	ig;

	length = list_length(*b);
	ig = 0;
	while (*b)
	{
		tmp = fill_tmp_tab(*b, 1);
		sg = tmp[list_length(*b) - 2];
		next = greatest_elem(*b);
		if (ig && distance(*b, greatest_elem(*b)) > distance(*b, sg))
			next = sg;
		ig = next == greatest_elem(*b) ? 1 : 0;
		while ((*b)->data != next)
			rotate_inst(b, NULL, inst, at_start(*b, next)
					? "rb" : "rrb");
		if ((*b)->data == next)
			push_inst(b, a, inst, "pa");
		check_swap(a, b, inst);
		free(tmp);
	}
}

void			minmax_sort(t_swap **a, t_swap **b, t_inst **inst)
{
	int	*tmp;

	tmp = fill_tmp_tab(*a, 1);
	fill_to_b(a, b, inst, tmp);
	fill_to_a(a, b, inst);
	if (!final_check(*a, *b))
		merge_sort(a, b, inst);
	clean_rotation(inst);
	free(tmp);
}
