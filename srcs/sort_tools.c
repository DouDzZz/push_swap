/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 17:16:50 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/29 12:15:48 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		decile_value(int *tmp, int nb, int length)
{
	if (length / 50 > 5)
		return (tmp[(length / (length / 50)) * nb]);
	else
		return (tmp[(length / 5) * nb]);
}

int		next_at_start(t_swap **a, int chr)
{
	int		i;
	int		length;
	t_swap	*tmp;

	tmp = *a;
	i = 0;
	length = list_length(*a);
	while (tmp)
	{
		if (tmp->data < chr)
			return (i < length / 2);
		tmp = tmp->next;
		i++;
	}
	return (0);
}

int		still_under_decile(t_swap *list, int decile)
{
	t_swap	*tmp;

	tmp = list;
	while (tmp)
	{
		if (tmp->data <= decile)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
