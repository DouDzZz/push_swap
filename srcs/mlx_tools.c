/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_tools.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 11:31:44 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/03 15:25:09 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		clear_pixels(char *img)
{
	int	i;

	i = 0;
	while (i++ < (WIN_WIDTH * WIN_HEIGHT * 4))
		img[i] = 0;
}

void		print_color(char *img, int x, int y, int color)
{
	int	r;
	int	g;
	int	b;

	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	img[((x * 4) + 4 * WIN_WIDTH * y) + 2] = r;
	img[((x * 4) + 4 * WIN_WIDTH * y) + 1] = g;
	img[((x * 4) + 4 * WIN_WIDTH * y) + 0] = b;
}

static void	print_list_head(t_swap *list, t_visu params, int height)
{
	char	*data;

	if (list)
	{
		data = ft_itoa(list->data);
		mlx_string_put(params.init, params.window,
			list->data < 0 ? 4259 : 435, height, params.colors.green_txt, data);
		ft_strdel(&data);
	}
	if (!params.colors.gradient)
	{
		mlx_string_put(params.init, params.window,
				500, LINTER, params.colors.i_data, "this color if < 0");
		mlx_string_put(params.init, params.window,
				500, HINTER, params.colors.s_data, "this color if > 0");
	}
	mlx_string_put(params.init, params.window,
			1010, LINTER, params.colors.green_txt, "[space] pause");
	mlx_string_put(params.init, params.window,
			1010, HINTER, params.colors.green_txt, "  [esc] quit");
	mlx_string_put(params.init, params.window,
			800, LINTER, params.colors.green_txt, "[1-5/g] color");
	mlx_string_put(params.init, params.window,
			800, HINTER, params.colors.green_txt, "    [b] dark mode");
}

static void	brackets_message(t_swap *a, t_swap *b, t_visu *params)
{
	mlx_string_put(params->init, params->window,
			250, MINTER, params->colors.green_txt, "[");
	if (final_check(a, b))
	{
		mlx_string_put(params->init, params->window,
				260, MINTER, params->colors.green_txt, "SORTED!");
		mlx_put_image_to_window(params->init,
			params->window, params->good_job,
			(WIN_WIDTH / 2) + (WIN_WIDTH / 6), 50);
	}
	else if (params->pause)
		mlx_string_put(params->init, params->window,
				260, MINTER, params->colors.red_txt, " PAUSE ");
	else
		mlx_string_put(params->init, params->window,
				260, MINTER, params->colors.green_txt, "SORTING");
	mlx_string_put(params->init, params->window,
			330, MINTER, params->colors.green_txt, "]");
}

void		interface(t_visu *params, char *str, t_swap *a, t_swap *b)
{
	char	*max;
	char	*moves;
	int		green;

	green = params->colors.green_txt;
	moves = ft_itoa(params->moves + 1);
	max = ft_itoa(params->max_moves);
	mlx_put_image_to_window(params->init, params->window, params->image, 0, 0);
	mlx_string_put(params->init, params->window, 10, MINTER, green, "Moves: ");
	mlx_string_put(params->init, params->window,
			140 - ft_strlen(moves) * 10, MINTER, green, moves);
	mlx_string_put(params->init, params->window, 150, MINTER, green, "/");
	mlx_string_put(params->init, params->window, 170, MINTER, green, max);
	mlx_string_put(params->init, params->window, 350, MINTER, green, str);
	mlx_string_put(params->init, params->window, 400, LINTER, green, "A: ");
	mlx_string_put(params->init, params->window, 400, HINTER, green, "B: ");
	print_list_head(a, *params, LINTER);
	print_list_head(b, *params, HINTER);
	mlx_hook(params->window, 2, 1L << 0, deal_key, params);
	brackets_message(a, b, params);
	ft_strdel(&moves);
	ft_strdel(&max);
}
