/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inst.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:00:35 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/11 12:03:03 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_inst(t_swap **src, t_swap **dest, t_inst **inst, char *op)
{
	push(src, dest);
	push_back_inst(inst, op);
}

void	rotate_inst(t_swap **a, t_swap **b, t_inst **inst, char *op)
{
	if (!ft_strcmp(op, "rra") || !ft_strcmp(op, "rrb"))
	{
		reverse_rotate(a);
		push_back_inst(inst, op);
	}
	else if (!ft_strcmp(op, "ra") || !ft_strcmp(op, "rb"))
	{
		rotate(a);
		push_back_inst(inst, op);
	}
	else if (!ft_strcmp(op, "rrr"))
	{
		rotate_both(a, b);
		push_back_inst(inst, op);
	}
	else if (!ft_strcmp(op, "rr"))
	{
		reverse_rotate_both(a, b);
		push_back_inst(inst, op);
	}
}

void	swap_inst(t_swap **list, t_inst **inst, char *op)
{
	swap_firsts(list);
	push_back_inst(inst, op);
}

void	swap_both_inst(t_swap **a, t_swap **b, t_inst **inst, char *op)
{
	swap_both(a, b);
	push_back_inst(inst, op);
}
