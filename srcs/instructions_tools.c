/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_tools.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 19:14:08 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/25 17:07:24 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_inst	*create_inst(char *data)
{
	t_inst	*new;

	if (!(new = (t_inst*)malloc(sizeof(t_inst))))
		return (NULL);
	new->data = data;
	new->next = NULL;
	return (new);
}

void	push_back_inst(t_inst **begin_list, char *data)
{
	t_inst	*new;
	t_inst	*tmp;

	tmp = *begin_list;
	if (tmp == NULL)
	{
		*begin_list = create_inst(data);
		return ;
	}
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = create_inst(data);
	tmp->next = new;
}

void	del_first_inst(t_inst **begin_list)
{
	t_inst	*tmp;

	if (*begin_list == NULL)
		return ;
	tmp = (*begin_list);
	*begin_list = (*begin_list)->next;
	free(tmp);
}

int		inst_length(t_inst *inst)
{
	int		i;
	t_inst	*tmp;

	i = 1;
	tmp = inst;
	while (tmp && tmp->next)
	{
		tmp = tmp->next;
		i++;
	}
	return (i);
}
