/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 11:34:24 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/11 11:51:53 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	test_clean(t_swap *a, t_swap *b, t_inst **tmp, t_inst **inst)
{
	rewrite_inst(tmp, *inst);
	clean_inst(tmp);
	if (checker_tmp(a, b, *tmp))
		rewrite_inst(inst, *tmp);
}

static void	continue_testing(t_swap *a, t_swap *b, t_inst **inst)
{
	t_swap	*tmpa;
	t_swap	*tmpb;
	t_inst	*tmp;

	tmpa = NULL;
	tmpb = NULL;
	tmp = NULL;
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	minmax_sort(&tmpa, &tmpb, &tmp);
	if ((!checker_tmp(tmpa, tmpb, *inst) && checker_tmp(tmpa, tmpb, tmp))
			|| inst_length(tmp) < inst_length(*inst))
		rewrite_inst(inst, tmp);
	dellists(&tmpa, &tmpb, &tmp);
}

static void	test_multiple_times(t_swap *a, t_swap *b, t_inst **inst)
{
	t_swap		*tmpa;
	t_swap		*tmpb;
	t_inst		*tmp;

	tmpa = NULL;
	tmpb = NULL;
	tmp = NULL;
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	basic_sort(&tmpa, &tmpb, &tmp);
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	merge_sort(&tmpa, &tmpb, inst);
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	if ((!checker_tmp(tmpa, tmpb, *inst) && checker_tmp(tmpa, tmpb, tmp))
			|| inst_length(tmp) < inst_length(*inst))
		rewrite_inst(inst, tmp);
	test_clean(tmpa, tmpb, &tmp, inst);
	copy_swap(&tmpa, a);
	copy_swap(&tmpb, b);
	continue_testing(a, b, inst);
	dellists(&tmpa, &tmpb, &tmp);
}

int			main(int ac, char **av)
{
	int		i;
	t_swap	*a;
	t_swap	*b;
	t_inst	*inst;

	a = NULL;
	b = NULL;
	inst = NULL;
	if (check_input(ac, av) == -1)
		return (ft_print_error());
	else if (check_input(ac, av) == 0)
		return (0);
	i = 0;
	while (++i < ac)
		create_swap_list(&a, av[i]);
	if (!check_doublons(a))
		return (ft_print_error());
	if (list_length(a) < 4)
		small_sort(&a, &inst);
	else
		test_multiple_times(a, b, &inst);
	print_instructions(inst);
	dellists(&a, &b, &inst);
	return (0);
}
