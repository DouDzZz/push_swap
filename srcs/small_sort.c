/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 17:29:21 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/28 12:40:45 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	three_list_length(t_swap **a, t_inst **inst)
{
	if ((*a)->data == smallest_elem(*a)
			&& (*a)->next->data == greatest_elem(*a))
	{
		rotate_inst(a, NULL, inst, "rra");
		swap_inst(a, inst, "sa");
	}
	else if ((*a)->next->data == smallest_elem(*a)
			&& (*a)->next->next->data == greatest_elem(*a))
		swap_inst(a, inst, "sa");
	else if ((*a)->next->data == greatest_elem(*a)
			&& (*a)->next->next->data == smallest_elem(*a))
		rotate_inst(a, NULL, inst, "rra");
	else if ((*a)->data == greatest_elem(*a)
			&& (*a)->next->data == smallest_elem(*a))
		rotate_inst(a, NULL, inst, "ra");
	else if ((*a)->data == greatest_elem(*a)
			&& (*a)->next->next->data == smallest_elem(*a))
	{
		swap_inst(a, inst, "sa");
		rotate_inst(a, NULL, inst, "rra");
	}
}

void		small_sort(t_swap **a, t_inst **inst)
{
	if (list_length(*a) == 1)
		return ;
	else if (list_length(*a) == 2)
	{
		if (!is_ascendant(*a))
			swap_inst(a, inst, "sa");
	}
	else if (list_length(*a) == 3)
		three_list_length(a, inst);
}
