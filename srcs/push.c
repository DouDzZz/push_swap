/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 16:46:05 by edjubert          #+#    #+#             */
/*   Updated: 2019/04/11 12:07:21 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	push(t_swap **src, t_swap **dest)
{
	if (*src == NULL)
		return (0);
	ft_push_swap(dest, (*src)->data, (*src)->index);
	ft_del_first_swap(src);
	return (1);
}

int	distance(t_swap *list, int chr)
{
	int		i;
	t_swap	*tmp;

	i = 0;
	tmp = list;
	while (tmp)
	{
		if (tmp->data == chr)
		{
			return (i > list_length(list) / 2
					? ft_abs(i - list_length(list)) : i);
		}
		tmp = tmp->next;
		i++;
	}
	return (0);
}
